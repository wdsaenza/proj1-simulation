#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cmath>

using namespace std;
const int N=217;

int main (int argc, char** argv)
{
  int i,j;
  int histogram[N];
  for (i=0; i<N; i++)
    histogram[i]=0;
  bool bloq=false,cond=false;
  double colA,colB;
  string line;
  ifstream myfile1 (argv[1]);
  if (myfile1.is_open())
    {
      while ( getline (myfile1,line) )
	{
	  if (bloq==true){
	    if (line.find("time")!=0){
	      stringstream(line) >> colA >> colB;
	      histogram[int(colB)]+=1;
	      cond=true;
	      //cout<<colA<<"\t"<<colB<<endl;
	    }
	  }
	  if (line.find("time")==0){
	    if (cond==true){
	      for (i=1; i<N; ++i)
		cout << i << "\t" << histogram[i] << endl;
	      for (i=1; i<N; ++i)
		histogram[i]=0;
	    }
	    cout << "\n\n\"" << line << "\"" << endl;
	    bloq=true;
	  }
	}
      myfile1.close();
    }
  return 0;
}
