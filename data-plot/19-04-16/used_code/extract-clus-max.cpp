#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cmath>

using namespace std;
const int N=217;

int main (int argc, char** argv)
{
  int i,j;
  int histogram[N], hisFin[N];
  for (i=0; i<N; i++){
    hisFin[i]=0;
    histogram[i]=0;
  }
  bool bloq=false,cond=false;
  double colA,colB;
  string line;
  ifstream myfile1 (argv[1]);
  if (myfile1.is_open()){
    while ( getline (myfile1,line) ){
      if (bloq==true)
	if (line.find("\"time")!=0){
	  stringstream(line) >> colA >> colB;
	  histogram[int(colA)]=colB;
	  cond=true;
	}
      
      if (line.find("\"time")==0){
	if (cond==true)
	  for (i=1; i<N; ++i){
	    hisFin[i]=histogram[i];
	    histogram[i]=0;
	  }
	bloq=true;
      }
    }
    myfile1.close();
  }
  
  double ClusMax=0;
  for (i=1; i<N; ++i)
    if(i>ClusMax && hisFin[i]==1)
      ClusMax=i;
  cout<<"-"<<argv[2]<<"\t"<<ClusMax<<endl;
  return 0;
}
