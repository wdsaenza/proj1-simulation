unset key
set xr [-4.1:-0.7]; set yr [-0.1:1.2]
set ytics 1
set xtics(-0.8, -1.0, -1.2, -1.4, -1.6, -1.8, -2.0, -2.2, -2.4, -2.6, -2.8, -3.0, -3.2, -3.4, -3.6, -3.8, -4.0)

set style line 1 lc rgb '#0060ad' lt 1 lw 1 pt 7 ps 1

set xl 'Energía inicial'
set yl 'Número de clusters con $N=3$ en $t=6000$'

plot for [i in "0.8 1.0 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0 3.2 3.4 3.6 3.8 4.0"] 'ene0-'.i.'New' i 50 u (0-i):2 every ::4::4 ls 1
