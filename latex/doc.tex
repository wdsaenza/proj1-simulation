\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{braket}
\usepackage{caption}
\usepackage{color}
\usepackage{float}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{subfigure}
\usepackage[colorlinks=true,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\usepackage[left=3cm,right=2cm,top=1.4cm,bottom=1.5cm]{geometry}
\raggedbottom
\decimalpoint
\captionsetup{width=.6\textwidth, font=scriptsize, labelfont=bf,
  singlelinecheck=off, justification=centering}

\begin{document}
\title{\textbf{Proyecto 1 - Métodos de simulación física: Fragmentación y Colisión de Gotas Clásicas}}
\author{\begin{small}\textbf{$^{1}$Andrés Navarro Alsina, $^{1}$William Saenz Arevalo} 
\end{small}\\\begin{scriptsize}
$^{1}$Universidad Nacional de Colombia.
\end{scriptsize}}
\date{}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Resumen}

Se utilizó el método de la dinámica molecular para estudiar la fragmentación y colisión de gotas clásicas. Las gotas fueron idealizadas como clusters de $N$ elementos, cuyo mecanismo de interacción entre pares viene dado por un potencial Lennard-Jones. La construcción de los clusters iniciales, con ciertos valores de energía y densidad, se logró durante una primera etapa en la que los $N$ elementos fueron comprimidos y enfriados hasta alcanzar las condiciones necesarias. Posteriormente, ya sea que hubiera colisión entre gotas o que se considerara la evaporación de una sola, se analizó la distribución de partículas expulsadas y formación de otros clusters al variar las energías iniciales de las gotas.

\section{Introducción}

Dentro de los varios modelos fenomenológicos de la física nuclear se encuentra el modelo de gota líquida, el cual predice propiedades y aspectos nucleares tales como la fórmula semi-empírica de las masas, energías de ligadura promedio entre nucleones, energías de separación para protones y neutrones, entre otros \cite{krane}. Este modelo surge de forma directa al intentar hacer una analogía entre las energías que determinan las características de una gota líquida y un núcleo atómico, interviniendo términos para la energía superficial, volumétrica y de Coulomb.

Por otro lado, es común encontrar modelos computacionales en el régimen de la dinámica molecular (DM) para fluidos en las fases líquida y vapor, descritos por potenciales tipo Lennard-Jones (L-J) \cite{verlet} \cite{andrij}. De esta forma, y debido a la analogía presentada por el modelo de gota, nace la idea de estudiar fenómenos de la física nuclear (como lo es la colisión de iones) mediante la simulación de clusters ensamblados bajo la interacción de estos potenciales tipo L-J.

\section{Método de simulación}

La dinámica molecular del conjunto de partículas fue determinada por el método de Verlet mejorado \cite{omelyan}, y se basó en primera aproximación a las constantes de simulación y modelos de interacción propuestos en \cite{art1} y \cite{art2}.

\subsection{Constantes y modelo de interacción}

El mecanismo básico de interacción, es decir el potencial L-J truncado para $r>3$, se expresa como
\begin{align*}
V(r<3)&=4(r^{-12}-r^{-6})-4(3^{-12}-3^{-6}), \\
V(r>3)&=0
\end{align*}
en donde $r$ es la distancia entre las partículas interactuantes, y el término $4(3^{-12}-3^{-6})$ es agregado para asegurar la continuidad del potencial en $r=3$; la figura \ref{f1} muestra gráficamente la forma de este potencial. La unidad de energía en la que se expresan los resultados a lo largo de este trabajo es 119.8 K, la unidad de distancia es 3.405 $\buildrel _\circ \over {\mathrm{A}}$ y las masas de las partículas se escogen iguales a las de un átomo de argón: 39.948 u.m.a. Con la elección anterior de constantes queda definida la unidad de tiempo como 10$^{-14}$ s, no obstante el paso de tiempo $\Delta t$ en la simulación se escoge de 10$^{-2}$, es decir que en unidades reales $\Delta t\rightarrow 10^{-16}$ s. En general, las simulaciones se terminan cuando $t\approx6000$. 

\begin{figure}[h!]
    \input{LJ6-12}
\centering
\caption{Forma del potencial L-J implementado en la simulación.}
\label{f1}
\end{figure}

\subsection{Condiciones iniciales}

Para tener control de las características de los núcleos (clusters iniciales) estudiados en la fragmentación y colisión, se implementó una primera etapa en donde se ``moldea'' el conjunto de partículas hasta llevarlo a ciertos valores de energía y densidad. En forma de receta, a continuación se enumeran los pasos seguidos para lograr este objetivo:
\begin{enumerate}
\item [$i$)] Se inicializa una red tridimensional perfecta de partículas en una región cubica determinada por seis paredes\footnote{La interacción entre las partículas y las paredes está expresada por una fuerza tipo Hertz, es decir que las paredes en realidad son esferas cuyo radio es mucho más grande que cualquier distancia considerada en la simulación.}. Las velocidades iniciales de cada una de las partículas se escoge de forma aleatoria (dirección aleatoria y magnitud fija) tal que al sumar las energías cinéticas individuales se llegue a $E_{K_0}$.
\item [$ii$)] Durante un intervalo de tiempo $\delta t_1\approx 50$, se reducen las dimensiones de la región cubica, es decir las paredes se acercan entre sí hasta alcanzar un volumen $V_0$. Dependido del número de partículas totales $N$, este volumen inicial se calcula para obtener densidades $\rho_0=N/V_0\approx0.8$.
\item [$iii$)] Debido a que la compresión es no adiabática, pues las paredes aumentan la energía cinética promedio de las partículas, se hace una recalibración progresiva de las magnitudes de las velocidad (más no de las direcciones) durante un intervalo de tiempo $\delta t_2\approx 10$. En este paso, lo que se hace es multiplicar los vectores velocidad $\vec{v}_i$ por un número menor a 1 (mayor a 1) si la energía total\footnote{La energía total corresponde a la sume de las energías cinéticas individuales más la energía potencial del cluser debida a la interacción entre partículas y de estas contra las fronteras.} es superior (inferior) al valor deseado $E_{T_0}$.
\item [$iv$)] Se eliminan las paredes y se deja evolucionar el sistema durante un intervalo de tiempo $\delta t_3\approx10$, esto con el objetivo de que la energía superficial genere una forma esférica aproximada del cluster.
\end{enumerate}

\subsection{Definición de clusters}

Andrés, explique!

\subsection{Dinámica de las colisiones}

\section{Resultados}

\begin{figure}[h!]
\hspace{-0.7cm}    \input{espec}
\centering
\vspace{-1.5cm}
\caption{Espectros de velocidad para los monómeros producto de la colisión.}
\label{fig:espec}
\end{figure}


\begin{figure}[H]
    \input{mono}
\centering
\label{f2}
\caption{Cantidad de monómeros en tiempo estacionario para distintas energías iniciales. Se muestra el resultado de tres simulaciones, cada una con una semilla de números aleatorios distinta.}
\end{figure}

\begin{figure}[H]
    \input{dime}
\centering
\label{f3}
\caption{Cantidad de dimeras en tiempo estacionario para distintas energías iniciales. Se muestra el resultado de tres simulaciones, cada una con una semilla de números aleatorios distinta.}
\end{figure}

\begin{figure}[H]
    \input{clusMay}
\centering
\label{f4}
\caption{Tamaño del cluster mayor en tiempo estacionario para distintas energías iniciales. Se muestra el resultado de tres simulaciones, cada una con una semilla de números aleatorios distinta.}
\end{figure}

\begin{figure}[H]
    \input{mayVSt}
\centering
\label{f5}
\caption{Evolución en el tiempo del número de partículas que componen el cluster mayor. Resultado para distintas energías iniciales $E_{T_0}$.}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{thebibliography}{X}

\bibitem{krane}
K. Kreine, Introductory Nuclear Physics. Wiley $\&$ Sons, 1988.

\bibitem{verlet}
L. Verlet, \textit{Computer ``Experiments'' on Classical Fluids. I. Thermodynamical Properties of Lennard-Jones Molecules}, \textbf{Phys. Rev.} 159, 1. 1967.

\bibitem{andrij}
A. Trokhymchuk, J. Alejandre, \textit{Computer simulations of liquid/vapor interface in Lennard-Jones fluids: Some questions and answers}, \textbf{Jour. Chem. Phys.} 111, 18. 1999.

\bibitem{omelyan}
I. P. Omelyan, I. M. Mryglod, and R. Folk, \textit{Optimized Verlet-like algorithms for molecular dynamics simulations}, \textbf{Phys. Rev. E} 65, 5. 2002.

\bibitem{art1}
A. Vicentini, G. Jacucci, and V. R. Pandharipande, \textit{Fragmentation of hot classical drops}, \textbf{Phys. Rev. C} 31, 5. 1984.

\bibitem{art2}
T. J. Schlagel, and V. R. Pandharipande, \textit{Classical simulation of heavy-ion collisions}, \textbf{Phys. Rev. C} 36, 1. 1986.

\end{thebibliography}

\end{document}
