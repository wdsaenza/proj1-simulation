unset key
set xr [-10:10]
set yr [-10:10]

set parametric
set isosamples 8,7                 # curvas de contorno
set view equal xyz                 # scale -1
set hidden3d                       # pintura transparente
set pal gray                       # pintura gris
set pm3d depthorder hidden3d       # algo con el fondo
set pm3d implicit                  # usar pm3d
unset colorbox                     # no usar indicador magnitud-color

set urange [0:2*pi]
set vrange [-pi/2:pi/2]
r=1.0
fx(v,u) = r*cos(v)*cos(u)
fy(v,u) = r*cos(v)*sin(u)
fz(v)   = r*sin(v)
splot fx(v,u),fy(v,u),fz(v) w l lc 8,\
      
