#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cmath>

using namespace std;

int main (int argc, char** argv)
{
  int i;
  double Max=2, Min=0;
  double Rango=Max-Min;
  double Ancho=0.02;
  double LadoInf=Min;
  int N=int(Rango/Ancho);
  int histogram[N];
  for (i=0; i<N; i++)
    histogram[i]=0;
  double colA;
  string line;
  ifstream myfile1 (argv[1]);

  if (myfile1.is_open()){
    while ( getline (myfile1,line) )
      {
	stringstream(line) >> colA;
	for(i=0;i<N;i++)
	  if( colA>Min+(i*Ancho) && colA<=Min+((i+1)*Ancho)){
	    histogram[i]+=1;
	    break;
	  }
      }
    myfile1.close();
  }
  int j=0;
  for (i=0; i<N; ++i)
    cout << Min+(i*Ancho) << "\t" << histogram[i] << endl;

  return 0;
}
