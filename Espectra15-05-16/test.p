binstart=0
binwidth=0.02
set xr [0:2]
set multiplot layout 3,1 columnsfirst
plot 'monoE4.dat' i 0 u (binwidth*(floor(($1-binstart)/binwidth)+0.5)+binstart):(1.0) smooth freq w boxes t '4a',\
     'MonoE4.dat' t '4b'
plot 'monoE6.dat' i 0 u (binwidth*(floor(($1-binstart)/binwidth)+0.5)+binstart):(1.0) smooth freq w boxes t '6a',\
     'MonoE6.dat' t '6b'
plot 'monoE10.dat' i 0 u (binwidth*(floor(($1-binstart)/binwidth)+0.5)+binstart):(1.0) smooth freq w boxes t '10a',\
     'MonoE10.dat' t '10b'
