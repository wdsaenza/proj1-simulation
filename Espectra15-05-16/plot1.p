m=39.948
set xr [0:2]; set yr [0:900]
f1(x)=A1*(x**2)*(exp(-B1*x*x))
A1=3000;B1=3
f2(x)=A2*(x**2)*(exp(-B2*x*x))
A2=3000;B2=3
f3(x)=A3*(x**2)*(exp(-B3*x*x))
A3=3000;B3=3
f4(x)=A4*(x**2)*(exp(-B4*x*x))
A4=3000;B4=3
fit [0:2] f1(x) 'MonoE4.dat' via A1,B1
fit [0:2] f2(x) 'MonoE6.dat' via A2,B2
fit [0:2] f3(x) 'MonoE8.dat' via A3,B3
fit [0:2] f4(x) 'MonoE10.dat' via A4,B4

set terminal epslatex size 6.5,5.2 color colortext
set output 'espec.tex'

# MACROS
NOXTICS = "set xtics ('' 0,'' 0.4,'' 0.8,'' 1.2,'' 1.6,'' 2.0); \
          unset xlabel"
XTICS = "set xtics (0.4,0.8,1.2,1.6);\
          set xlabel '$v$'"
NOYTICS = "set ytics ('' 200,'' 400,'' 600,'' 800); \
          unset ylabel"
YTICS = "set ytics (200,400,600,800);\
          set ylabel 'Cuentas/0.02'"
# Margins for each row resp. column
TMARGIN = "set tmargin at screen 0.90; set bmargin at screen 0.55"
BMARGIN = "set tmargin at screen 0.55; set bmargin at screen 0.20"
LMARGIN = "set lmargin at screen 0.15; set rmargin at screen 0.55"
RMARGIN = "set lmargin at screen 0.55; set rmargin at screen 0.95"
# Placement of the a,b,c,d labels in the graphs
POS = "at graph 0.7,0.5"

### Start multiplot (2x2 layout)
set multiplot layout 2,2 rowsfirst
# --- GRAPH a
@TMARGIN; @LMARGIN
@NOXTICS; @YTICS
set label 1 '$E_{T_0}=4$' @POS
plot 'MonoE4.dat' w his lw 3 lc 2 t '\small{Simulación}',\
     f1(x) lw 2 lc 1 t '\small{Ajuste}'
# --- GRAPH b
@TMARGIN; @RMARGIN
@NOXTICS; @NOYTICS
set label 1 '$E_{T_0}=6$' @POS
plot 'MonoE6.dat' w his lw 3 lc 2 t '\small{Simulación}',\
     f2(x) lw 2 lc 4 t '\small{Ajuste}'
# --- GRAPH c
@BMARGIN; @LMARGIN
@XTICS; @YTICS
set label 1 '$E_{T_0}=8$' @POS
plot 'MonoE8.dat' w his lw 3 lc 2 t '\small{Simulación}',\
     f3(x) lw 2 lc 6 t '\small{Ajuste}'
# --- GRAPH d
@BMARGIN; @RMARGIN
@XTICS; @NOYTICS
set label 1 '$E_{T_0}=10$' @POS
plot 'MonoE10.dat' w his lw 3 lc 2 t '\small{Simulación}',\
     f4(x) lw 2 lc 7 t '\small{Ajuste}'
unset multiplot

